from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from receipts.models import ExpenseCategory, Receipt, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.


@login_required
def receipt_list_view(request):
    context = {"receipts": Receipt.objects.filter(purchaser=request.user)}
    return render(request, "receipts/home.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            specific_receipt = form.save(False)
            specific_receipt.purchaser = request.user
            specific_receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"form": form}
    return render(request, "receipts/create_receipt.html", context)


@login_required
def expense_category_list_view(request):
    user_expense_categories = ExpenseCategory.objects.filter(
        owner=request.user
    )
    context = {"expense_categories": user_expense_categories}
    return render(request, "receipts/expense_category_list.html", context)


@login_required
def account_list_view(request):
    user_accounts = Account.objects.filter(owner=request.user)
    print(type(user_accounts))
    context = {"accounts": user_accounts}
    return render(request, "receipts/account_list.html", context)


@login_required
def create_expense_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            specific_expense_category = form.save(False)
            specific_expense_category.owner = request.user
            specific_expense_category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {"form": form}
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            specific_account = form.save(False)
            specific_account.owner = request.user
            specific_account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {"form": form}
    return render(request, "receipts/create_account.html", context)
